const getSum = (str1, str2) => {
  if (str1 === '') {
    str1 = '0';
  }

  if (str2 === '') {
    str2 = '0';
  }

  if (typeof str1 !== "string" || typeof str2 !== "string" || Number.isNaN(Number(str1)) || Number.isNaN(Number(str2))) {
    return false;
  }
  else {
    let result = Number(str1) + Number(str2);
    return result.toString();
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  
  let filteredPosts = 0;
  let filteredComments = 0;
  for (let post of listOfPosts) {
    if (post.author == authorName) {
      filteredPosts++;
    }
    let commentsCount = post.comments;
    if(commentsCount === undefined)
    {
      continue;
    }
    for (let j = 0; j < commentsCount.length; j++) {
      let comment = post.comments[j];
      if (comment.author == authorName) {
        filteredComments++;
      }

    }
  }
  return "Post:" + filteredPosts + ",comments:" + filteredComments;
};

const tickets = (people) => {
  var cash = [];
  for (const bill of people) {
    let payment = Number(bill);
    if (payment % 25 !== 0) {
      return "NO";
    }
    cash.push(payment)
    let index;
    switch (payment) {
      case 50:
        if (!cash.includes(25)) {
          return "NO";
        }
        else {
          index = cash.indexOf(25);
          cash.splice(index, 1);
        }
        break;
      case 100:
        let quaterCount = cash.filter((element) => element == 25);
        let halfCount = cash.filter((element) => element == 50);


        if (quaterCount.length > 2) {
          index = cash.indexOf(25);
          cash.splice(index, 3);
        }

        else if (quaterCount.length > 0 && halfCount.length > 0) {
          index = cash.indexOf(25);
          cash.splice(index, 1);
          index = cash.indexOf(50);
          cash.splice(index, 1);
        }

        else {
          return "NO";
        }
        break;

      default:
        break;
    }

  }
  return "YES";
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
